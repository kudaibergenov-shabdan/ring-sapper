import React from 'react';
import Cell from "../Cell/Cell";
import './Board.css';

const Board = props => {
    return (
        <div className="board">
            {props.cells.map((cell) => (
                <Cell key={cell.id}
                      cell={cell}
                      onCellClick={
                          () =>
                              {
                                  props.onBoardCellClick(cell.id)
                              }
                      }/>
            ))}
        </div>
    )
};

export default Board;