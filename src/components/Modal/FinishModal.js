import React from 'react';
import './FinishModal.css';

const FinishModal = props => {
    const modalClasses = (props.showModal) ? 'modal show': 'modal';

    return (
        <div className={modalClasses} id="modal">
            <div className="alert">
                <h4>Congratulations</h4>
                <p>Ring has been found !</p>
                <button className="btn-close" onClick={props.closeModal}>Close</button>
            </div>
        </div>
    );
};

export default FinishModal;