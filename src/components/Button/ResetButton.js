import React from 'react';
import './ResetButton.css';

const ResetButton = props => {
    return (
        <div className="reset-block">
            <button onClick={props.resetCells}>Reset</button>
        </div>
    );
};

export default ResetButton;