import React from 'react';
import './Tries.css'

const Tries = props => {
    return (
        <p className="tries">Tries: {props.clickCount}</p>
    );
};

export default Tries;