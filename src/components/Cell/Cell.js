import React from 'react';
import './Cell.css'

const Cell = props => {
    const cellClasses = ["cell"];
    if (props.cell.opened) {
        cellClasses.push('cellOpened');
        if(props.cell.hasRing) {
            cellClasses.push('ring');
        }
    }

    return (
        <div className={cellClasses.join(' ')} onClick={props.onCellClick}>
        </div>
    );
};

export default Cell;