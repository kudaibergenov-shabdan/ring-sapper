import {useState} from "react";
import './App.css';
import Board from "./components/Board/Board";
import ResetButton from "./components/Button/ResetButton";
import Tries from "./components/Tries/Tries";
import FinishModal from "./components/Modal/FinishModal";

const App = () => {
    const initCells = () => {
        const cells = [];
        const randomRing = Math.floor(Math.random() * 35);
        for (let i = 0; i < 36; i++) {
            if (randomRing === i) {
                cells.push({id: i, hasRing: true, opened: false});
            } else {
                cells.push({id: i, hasRing: false, opened: false});
            }
        }
        return cells;
    };

    const [cells, setCells] = useState(initCells());
    const [clickCount, setClickCount] = useState(0);
    const [clickable, setClickable] = useState(true);
    const [showModal, setShowModal] = useState(false);

    const resetCalls = () => {
        const cells = initCells();
        setClickCount(0);
        setClickable(true);
        setCells(cells);
    };

    const clickOnCell = (cellId) => {
        let clickCountCopy = clickCount;
        if(clickable) {
            setCells(cells.map(cell => {
                if (cell.id === cellId) {
                    if (!cell.opened) {
                        clickCountCopy++;
                        setClickCount(clickCountCopy);
                    }
                    if (cell.hasRing) {
                        setClickable(false);
                        setShowModal(true);
                    }
                    return {...cell, opened: true};
                } else {
                    return cell;
                }
            }));
        }
    };

    const hideModal = () => {
        setShowModal(false);
    }

    return (
        <div>
            <div>
                <Board cells={cells} onBoardCellClick={clickOnCell}/>
                <Tries clickCount={clickCount}/>
                <div>
                    <ResetButton resetCells={resetCalls}/>
                </div>
            </div>
            <FinishModal showModal={showModal} closeModal={hideModal}/>
        </div>
    );
}

export default App;
